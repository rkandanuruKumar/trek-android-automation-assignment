package main.java.com.testAndroid.Pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.trekApp.AndroidTest.GenericMethodsLib.GenericreUsableMethods;
import io.appium.java_client.android.AndroidDriver;

public class TREKAndroidAppScreen1 {
    AndroidDriver driver = null;
    GenericreUsableMethods genMethods = new GenericreUsableMethods();
// Java class is page class where we can collect automation objects(Page Elements) of app specific to page/functionality.

    @FindBy(xpath = "//android.widget.TextView[@text='Believe in bikes']")
    public WebElement Screen1_BelieveInBikesText;


}
