package com.trekApp.AndroidTest.GenericMethodsLib;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.Setting;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.io.*;
import static io.appium.java_client.touch.offset.PointOption.point;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
//import java.time.Duration;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import io.appium.java_client.TouchAction;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Properties;
import java.util.UUID;
public class GenericreUsableMethods {
    public AndroidDriver driver = null;
    ExtentReports extent;
// This java class is for creating generic methods for better re usability in Test class.

    // Code creating driver to launch the app

    public AndroidDriver launchApp() throws IOException, InterruptedException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("noReset", "true");
        capabilities.setCapability("platformName ", "Android");
        capabilities.setCapability("deviceName", "02157df27126911e");
        capabilities.setCapability("appPackage", "r_and_d_1.trekbikes.com.trekrd1");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("appActivity", "r_and_d_1.trekbikes.com.trekrd1.MainActivity");
        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        //System.out.println("Driver while Launching<" + driver);
        driver.manage().timeouts().implicitlyWait(59000, TimeUnit.SECONDS);
        System.out.println("==========App Launched========");

        return driver;
    }

    // Code for script to wait until object is found.
    public void waitForScreenToLoad(AndroidDriver driver, WebElement element, int seconds){
        WebDriverWait wait = new WebDriverWait(driver,seconds);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    // Code for capturing screenshot of the app when needed during real time execution.
    public String CaptureScreen(AndroidDriver driver) {
        try {
            String timeStamp = new SimpleDateFormat("MMddyyyy_HHmmss").format(Calendar.getInstance().getTime());
            String screenshotsDir = System.getProperty("user.dir") + "/Report/Screenshots/";
            String filename = "AndroidTREKapp" + "_" + timeStamp + ".jpg";
            Path screenshotPath = Paths.get(screenshotsDir, filename);
            File SrcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(SrcFile, screenshotPath.toFile());

            return filename;
        } catch (Exception e) {
            System.out.println("Exception while taking screenshot " + e.getMessage());
            return e.getMessage();
        }
    }

    // Code to publish Pass result in the extent report.
    public void testpass(String expectedString, AndroidDriver driver, ExtentReports extent,
                         ExtentTest test) throws IOException {

        test.pass(expectedString, MediaEntityBuilder.createScreenCaptureFromPath(CaptureScreen(driver)).build());

    }
    // Code to publish Fail result in the extent report.
    public void testfail(String expectedString, AndroidDriver driver, ExtentReports extent,
                         ExtentTest test) throws IOException {

        test.fail(expectedString, MediaEntityBuilder.createScreenCaptureFromPath(CaptureScreen(driver)).build());

    }
    // Code to publish information step in the extent report, which helps reviewer for more clarity viewing the sequence of test execution.
    public void testinfo(String expectedString, AndroidDriver driver, ExtentReports extent,
                         ExtentTest test) throws IOException {

        test.info(expectedString, MediaEntityBuilder.createScreenCaptureFromPath(CaptureScreen(driver)).build());

    }
}
