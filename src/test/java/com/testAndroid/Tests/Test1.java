package test.java.com.testAndroid.Tests;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.trekApp.AndroidTest.GenericMethodsLib.ExtentManager;
import com.trekApp.AndroidTest.GenericMethodsLib.GenericreUsableMethods;
import main.java.com.testAndroid.Pages.TREKAndroidAppScreen1;
import io.appium.java_client.android.AndroidDriver;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.NoAlertPresentException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;
import java.util.Map;

// Java class is a Test class. This shows the collection of test suite of the app.
public class Test1 {

    private ExtentReports extent;
    private ExtentTest test;
    private AndroidDriver driver;

    GenericreUsableMethods genMethods = new GenericreUsableMethods();
    TREKAndroidAppScreen1 screen1 = new TREKAndroidAppScreen1();

    @BeforeSuite
    public void beforeClass() throws MalformedURLException {

        extent = ExtentManager.getExtent();

    }

    @BeforeMethod
    public void setUp() throws MalformedURLException {

        try {

            driver = genMethods.launchApp();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        } catch (Exception e) {

            System.out.println("Exception------->" + e.toString());
        }
    }

    // Test to validate if Believe In Bikes Text is displayed upon launch of the App.
    
    @Test
    public void TC001_LaunchScreen_Validation_BelieveInBikesText()
            throws UnsupportedEncodingException, IOException, SQLException, InterruptedException {

        try {

            test = extent.createTest("Believe In Bikes Validation", "Test Case to validate Believe In Bikes Text is displayed upon launch of the App");
            PageFactory.initElements(driver, screen1);
            Assert.assertEquals(screen1.Screen1_BelieveInBikesText.getText(), "Believe in bikes");
            genMethods.testpass(screen1.Screen1_BelieveInBikesText.getText()+" Text is displayed on the Launch screen",driver, extent, test);

        } catch (Exception e) {

           genMethods.testfail(screen1.Screen1_BelieveInBikesText.getText()+" Text is not found on the Launch screen",driver, extent, test);
            Assert.assertTrue(false);
            System.out.println("Exception------->" + e.toString());

        }
    }

    @AfterSuite
    public void afterClass() {

        extent.flush();
        driver.quit();

    }
}
